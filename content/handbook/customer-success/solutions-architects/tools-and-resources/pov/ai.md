---

title: Artificial Intelligence POV Scope and Acceptance
description: Artifical Intelligence POV Scope and Acceptance
---

## Pre requisite

We need to make sure the customer has gone through the AI hands-on [workshop](https://handbook.gitlab.com/handbook/customer-success/solutions-architects/tools-and-resources/workshop/) to have a great experience with AI POV. We also recommend building a technical close [plan](https://handbook.gitlab.com/handbook/customer-success/solutions-architects/sa-practices/technical-close-plan/) and getting agreement from your customer before the POV start.

## AI POV best practices

We need to proactively identify languages, testing questions and teams with them. Below are the best practices to review before the start of the POV:

- Small team size
- Languages which has a lot of open source contributions like Java, Python
- Proactive testing scenario
- Time boxed hands on POV with active guidance throughout
- Conduct a pre survey before the POV and post survey after the POV to gauge the success


### Input to the POV

- Example Customer [template](https://docs.google.com/presentation/d/1m1u65qa8oj0_hHTklnhWAjyHxR8euQ14/edit?usp=sharing&ouid=113388956697042742039&rtpof=true&sd=true)

### Results

### Tracking


### Other POV Scope and Acceptance

SA working with SAE and AE can define the POV scope with the customer, with alignment to the business values and the GitLab solution. For each solution, the typical scope and acceptances are listed for reference but the team should define the scope, time and execution with acceptance for each engagement.

- [DevSecOps](/handbook/customer-success/solutions-architects/tools-and-resources/pov/devsecops/)
- [Automated Software Delivery](/handbook/customer-success/solutions-architects/tools-and-resources/pov/automation/)
- [DevOps Platform cumulatively](/handbook/customer-success/solutions-architects/tools-and-resources/pov/platform/)






